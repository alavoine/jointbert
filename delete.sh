export DIRECTORY=/mnt/beegfs/home/alavoine/dibiso/jointbert-media/spill/ray_spilled_objects

if [ -d $DIRECTORY ]; then
	cd $DIRECTORY
	if (( "$(ls | wc -l)" > 10 )); then
  		rm "$(ls -t | tail -1)"
	fi
fi

