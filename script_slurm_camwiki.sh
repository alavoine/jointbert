#!/bin/bash

#SBATCH --job-name=joint_media
#SBATCH --nodes=1 # must be 1 if PBT is used to avoid checkpoint trouble
#SBATCH --cpus-per-task=20
#SBATCH --gres=gpu:3
#SBATCH --exclusive
#SBATCH --ntasks-per-node=1
#SBATCH --mail-user=nadege.alavoine@lisn.fr
#SBATCH --mail-type=END,FAIL

####################### VARIABLES FOR PRE-TRAINING AND PBT TRAINING ##########################
models="camembert-wiki-4gb" # "fralbert camembert camembert-large camembert-ccnet camembert-ccnet-4gb camembert-oscar-4gb"
datas="media_original media_speechbrain_full media_speechbrain_relax"
data_dir=$(pwd)"/data"
stop_it=100 # stop all iterations, be careful that it's > or = to max_epochs
min_epochs=5
max_epochs=100
pbt_max_time=-1 # in minutes, max time for PBT, -1 for no time constraint
nb_runs=3 # number of differents seed explored
nb_trials=6 # number of trials run in parallel for PBT
##############################################################################################

########################### SLURM x RAY SETUP CONFIGURATION ##################################

# activate conda env
source /mnt/beegfs/home/alavoine/.bashrc
source activate pytorch


# export directory to store Ray session temporary files (by default, is /tmp but may lack space)
export RAY_TEMP_DIR="/mnt/beegfs/home/alavoine/dibiso/jointbert-media/spill"

# export SLURM_GPUS_PER_TASK as option is not available
export SLURM_GPUS_PER_TASK=3


################# DO NOT CHANGE THINGS HERE UNLESS YOU KNOW WHAT YOU ARE DOING ###############
redis_password=$(uuidgen)
export redis_password

nodes=$(scontrol show hostnames $SLURM_JOB_NODELIST) # Getting the node names
nodes_array=($nodes)

node_1=${nodes_array[0]}
ip=$(srun --nodes=1 --ntasks=1 -w $node_1 hostname --ip-address) # making redis-address

if [[ $ip == *" "* ]]; then
  IFS=' ' read -ra ADDR <<<"$ip"
  if [[ ${#ADDR[0]} > 16 ]]; then
    ip=${ADDR[1]}
  else
    ip=${ADDR[0]}
  fi
  echo "We detect space in ip! You are using IPV6 address. We split the IPV4 address as $ip"
fi

port=6379
ip_head=$ip:$port
export ip_head
echo "IP Head: $ip_head"

echo "STARTING HEAD at $node_1"
srun --nodes=1 --ntasks=1 -w $node_1 rm -rf /tmp/ray && \
  ray start --head --node-ip-address=$ip --port=6379 --redis-password=$redis_password --block \
  --num-cpus "${SLURM_CPUS_PER_TASK}" --num-gpus "${SLURM_GPUS_PER_TASK}" \
  --system-config='{"object_spilling_config":"{\"type\":\"filesystem\",\"params\":{\"directory_path\":\"'${RAY_TEMP_DIR}'\"}}"}' & \
sleep 30

worker_num=$(($SLURM_JOB_NUM_NODES - 1)) #number of nodes other than the head node
for ((i = 1; i <= $worker_num; i++)); do
  node_i=${nodes_array[$i]}
  echo "STARTING WORKER $i at $node_i"
  srun --nodes=1 --ntasks=1 -w $node_i rm -rf /tmp/ray && \
  ray start --address $ip_head --redis-password=$redis_password --block \
  --num-cpus "${SLURM_CPUS_PER_TASK}" --num-gpus "${SLURM_GPUS_PER_TASK}" & \
  sleep 5
done

sleep 30
##############################################################################################

############################# PBT LAUNCHING ##################################################
### Notes : Pre-training on atis_fr without PBT ##############################################
mkdir -p logs

echo "--- LAUNCHING PBT TRAINING --- "
echo -e "Note: No pre-training\n"
echo -e "Parameters:"
echo -e "\t - Models used : $models"
echo -e "\t - Number of runs (different seeds per experiment): $nb_runs"
echo -e "\t - Data used for PBT : $datas"
echo -e "\t - Number of iteration for PBT : From $min_epochs to $max_epochs or $max_epochs_big (big) or $stop_it (forced stop)  "
echo -e "\t - Number of trials per PBT experiment : $nb_trials"
echo -e "\t - Number of runs (seeds) to perform : $nb_runs"
echo -e "\t - Max time for a PBT run: $pbt_max_time min\n\n"

# Set verbosity for transformers only on errors (no more warning because new trainer is uninitialized for some weights)
export TRANSFORMERS_VERBOSITY=error
export TUNE_MAX_PENDING_TRIALS_PG=1 


for i in $(seq 1 $nb_runs);
do
	seed=$RANDOM
	for model in $models
	do
		mkdir -p ${model}
		for data in $datas
		do
      if [ ! -d "./${model}/${data}_model_${model}_seed_${seed}" ] 
			then
				echo "processing ${data}_model_${model}_seed_${seed} from $pt_model_dir with pbt"
				time python3 -u main_pbt.py --slurm --task ${data} --model_type ${model} --seed $seed --multilabel \
					--model_dir ${model}/${data}_model_${model}_seed_${seed} --do_train --do_eval --data_dir=${data_dir} \
					--train_batch_size -1 --learning_rate -1  --stop_time_min ${pbt_max_time} --num_samples ${nb_trials} \
          --cpu "${SLURM_CPUS_PER_TASK}" --gpu "${SLURM_GPUS_PER_TASK}" --stop_iterations ${stop_it} \
					--min_epochs ${min_epochs} --max_epochs ${max_epochs} >& ./logs/${data}_model_${model}_seed_${seed}.log
        rm -rf ray_results/${data}_joint_${model}_seed_${seed}
      fi
		done
	done
done




