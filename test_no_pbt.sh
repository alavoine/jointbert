#!/bin/bash

#SBATCH --job-name=joint_media
#SBATCH --nodes=1 # must be 1 if PBT is used to avoid checkpoint trouble
#SBATCH --cpus-per-task=12
#SBATCH --gres=gpu:1
#SBATCH --exclusive
#SBATCH --ntasks-per-node=1

# activate conda env
source /mnt/beegfs/home/alavoine/.bashrc
source activate pytorch

####################### VARIABLES FOR TRAINING ##########################
models="fralbert" # "fralbert camembert camembert-large camembert-ccnet camembert-ccnet-4gb camembert-oscar-4gb"
datas="media_original"
data_dir=$(pwd)"/data"
nb_epochs=5 # number of epochs to perform
nb_runs=1 # number of differents seed explored
lr=0.00005 # learning rate
tbs=32 # training batch size 

mkdir -p logs

echo "--- LAUNCHING TRAINING --- "
echo -e "Note: No pre-training\n"
echo -e "Parameters:"
echo -e "\t - Models used : $models"
echo -e "\t - Number of runs (different seeds per experiment): $nb_runs"
echo -e "\t - Data used : $datas"
echo -e "\t - Number of iteration : $nb_epochs"
echo -e "\t - Number of runs (seeds) to perform : $nb_runs"

# Set verbosity for transformers only on errors (no more warning because new trainer is uninitialized for some weights)
export TRANSFORMERS_VERBOSITY=error
export TUNE_MAX_PENDING_TRIALS_PG=1 

for i in $(seq 1 $nb_runs);
do
	seed=$RANDOM
	for model in $models
	do
		mkdir -p ${model}
		for data in $datas
		do
            if [ ! -d "./${model}/${data}_model_${model}_seed_${seed}" ] 
			then
				echo "processing ${data}_model_${model}_seed_${seed} from $pt_model_dir with pbt"
				time python3 -u main.py --task ${data} --model_type ${model} --seed $seed --multilabel \
					--model_dir ${model}/${data}_model_${model}_seed_${seed} --do_train --do_eval --data_dir=${data_dir} \
					--train_batch_size ${tbs} --learning_rate ${lr}  \
					--num_train_epochs ${nb_epochs} >& ./logs/${data}_model_${model}_seed_${seed}.log
			fi
		done
	done
done

