import os
import logging
from tqdm import tqdm

import numpy as np
import torch
from torch.utils.data import DataLoader, RandomSampler, SequentialSampler
from transformers import BertConfig, AdamW

from utils import MODEL_CLASSES, compute_metrics, get_intent_labels, get_slot_labels
from model.module import IntentClassifier, SlotClassifier

import ray
from ray.air import session
from ray.air.checkpoint import Checkpoint

logger = logging.getLogger(__name__)

def train_joint(config, args, train_dataset, dev_dataset, test_dataset):
    # Get dataset from ray id
    train_dataset = ray.get(train_dataset)
    dev_dataset = ray.get(dev_dataset)
    test_dataset = ray.get(test_dataset)

    # Intialize trainer
    args.dropout_rate = config["dropout_rate"]
    trainer = Trainer(args, train_dataset, dev_dataset, test_dataset)

    train_sampler = RandomSampler(train_dataset)
    train_dataloader = DataLoader(train_dataset, sampler=train_sampler, batch_size=config["train_batch_size"])

    # Prepare optimizer (weight decay)
    no_decay = ["bias", "LayerNorm.weight"]
    optimizer_grouped_parameters = [
            {'params': [p for n, p in trainer.model.named_parameters() if not any(nd in n for nd in no_decay)],
             'weight_decay': config["weight_decay"]},
            {'params': [p for n, p in trainer.model.named_parameters() if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
        ]

    optimizer = AdamW(optimizer_grouped_parameters, lr=config["learning_rate"], eps=config["adam_epsilon"], no_deprecation_warning=True)

    global_step = 0
    tr_loss = 0.0
    trainer.model.zero_grad()

    # Retrieve checkpoint from PBT scheduler if resume is possible
    loaded_checkpoint = session.get_checkpoint()
    if loaded_checkpoint:
        dict_checkpoint = loaded_checkpoint.to_dict()
        trainer.model.load_state_dict(dict_checkpoint["model_state_dict"])
        optimizer.load_state_dict(dict_checkpoint["optimizer_state_dict"])
        for idx, param in enumerate(optimizer.param_groups):
            param["weight_decay"] = optimizer_grouped_parameters[idx]["weight_decay"]
            param["lr"] = config["learning_rate"]
            param["eps"] = config["adam_epsilon"]

    # Train!
    while True: #infinity loop for PBT training
        epoch_iterator = tqdm(train_dataloader, desc="Training iteration", disable=True)

        for step, batch in enumerate(epoch_iterator):
            trainer.model.train()
            batch = tuple(t.to(trainer.device) for t in batch)  # GPU or CPU

            inputs = {
                "input_ids": batch[0],
                "attention_mask": batch[1],
                "intent_label_ids": batch[3],
                "slot_labels_ids": batch[4],
            }
            if args.model_type != "distilbert":
                inputs["token_type_ids"] = batch[2]
            outputs = trainer.model(**inputs)
            loss = outputs[0]

            if config["gradient_accumulation_steps"] > 1:
                loss = loss / config["gradient_accumulation_steps"]

            loss.backward()

            tr_loss += loss.item()
            if (step + 1) % config["gradient_accumulation_steps"] == 0:
                torch.nn.utils.clip_grad_norm_(
                    trainer.model.parameters(), config["max_grad_norm"]
                )
                optimizer.step()
                trainer.model.zero_grad()
                global_step += 1

        # Evaluation 
        results_train = evaluate(config, trainer, train_dataset, "train", multilabel=args.multilabel)
        results_dev = evaluate(config, trainer, dev_dataset, "dev", multilabel=args.multilabel)

        # Checkpointing and session.report for PBT
        checkpoint = Checkpoint.from_dict({"model_state_dict": trainer.model.state_dict(), "optimizer_state_dict": optimizer.state_dict()})
        if args.multilabel:
            session.report(
                {
                    "train_joint_loss": results_train["loss"],
                    "train_slot_precision": results_train["slot_precision"],
                    "train_slot_recall": results_train["slot_recall"],
                    "train_slot_f1": results_train["slot_f1"],
                    "train_slot_cer": results_train["slot_cer"],
                    "train_intent_acc": results_train["intent_acc"],
                    "train_intent_emr": results_train["intent_emr"],
                    "train_sem_fr_acc": results_train["sementic_frame_acc"],
                    "val_joint_loss": results_dev["loss"],
                    "val_slot_precision": results_dev["slot_precision"],
                    "val_slot_recall": results_dev["slot_recall"],
                    "val_slot_f1": results_dev["slot_f1"],
                    "val_slot_cer": results_dev["slot_cer"],
                    "val_intent_acc": results_dev["intent_acc"],
                    "val_intent_emr": results_dev["intent_emr"],
                    "val_sem_fr_acc": results_dev["sementic_frame_acc"],
                    "val_opti_metric": ((results_dev["slot_f1"] + results_dev["intent_acc"]) / 2), # mean of slot_f1 and intent_acc for optimization
                },
                checkpoint=checkpoint
            )
        else:
            session.report(
                {
                    "train_joint_loss": results_train["loss"],
                    "train_slot_precision": results_train["slot_precision"],
                    "train_slot_recall": results_train["slot_recall"],
                    "train_slot_f1": results_train["slot_f1"],
                    "train_slot_cer": results_train["slot_cer"],
                    "train_intent_acc": results_train["intent_acc"],
                    "train_sem_fr_acc": results_train["sementic_frame_acc"],
                    "val_joint_loss": results_dev["loss"],
                    "val_slot_precision": results_dev["slot_precision"],
                    "val_slot_recall": results_dev["slot_recall"],
                    "val_slot_f1": results_dev["slot_f1"],
                    "val_slot_cer": results_dev["slot_cer"],
                    "val_intent_acc": results_dev["intent_acc"],
                    "val_sem_fr_acc": results_dev["sementic_frame_acc"],
                    "val_opti_metric": ((results_dev["slot_f1"] + results_dev["intent_acc"]) / 2), # mean of slot_f1 and intent_acc for optimization
                },
                checkpoint=checkpoint
            )



def evaluate(config, trainer, dataset, mode, multilabel=False):
    eval_sampler = SequentialSampler(dataset)
    eval_dataloader = DataLoader(
        dataset, sampler=eval_sampler, batch_size=config["eval_batch_size"]
    )

    # Eval!
    logger.info("***** Running evaluation on %s dataset *****", mode)
    logger.info("  Num examples = %d", len(dataset))
    logger.info("  Batch size = %d", config["eval_batch_size"])
    eval_loss = 0.0
    nb_eval_steps = 0
    intent_preds = None
    slot_preds = None
    out_intent_label_ids = None
    out_slot_labels_ids = None

    trainer.model.eval()

    for batch in tqdm(eval_dataloader, desc="Evaluating", disable=True):
        batch = tuple(t.to(trainer.device) for t in batch)
        with torch.no_grad():
            inputs = {
                "input_ids": batch[0],
                "attention_mask": batch[1],
                "intent_label_ids": batch[3],
                "slot_labels_ids": batch[4],
            }
            if trainer.args.model_type != "distilbert":
                inputs["token_type_ids"] = batch[2]
            outputs = trainer.model(**inputs)
            tmp_eval_loss, (intent_logits, slot_logits) = outputs[:2]

            eval_loss += tmp_eval_loss.mean().item()
        nb_eval_steps += 1

        # Intent prediction
        if intent_preds is None:
            intent_preds = intent_logits.detach().cpu().numpy()
            out_intent_label_ids = inputs["intent_label_ids"].detach().cpu().numpy()
        else:
            intent_preds = np.append(
                intent_preds, intent_logits.detach().cpu().numpy(), axis=0
            )
            out_intent_label_ids = np.append(
                out_intent_label_ids,
                inputs["intent_label_ids"].detach().cpu().numpy(),
                axis=0,
            )

        # Slot prediction
        if slot_preds is None:
            if trainer.args.use_crf:
                # decode() in `torchcrf` returns list with best index directly
                slot_preds = np.array(trainer.model.crf.decode(slot_logits))
            else:
                slot_preds = slot_logits.detach().cpu().numpy()

            out_slot_labels_ids = inputs["slot_labels_ids"].detach().cpu().numpy()
        else:
            if trainer.args.use_crf:
                slot_preds = np.append(
                    slot_preds, np.array(trainer.model.crf.decode(slot_logits)), axis=0
                )
            else:
                slot_preds = np.append(
                    slot_preds, slot_logits.detach().cpu().numpy(), axis=0
                )

            out_slot_labels_ids = np.append(
                out_slot_labels_ids,
                inputs["slot_labels_ids"].detach().cpu().numpy(),
                axis=0,
            )

    eval_loss = eval_loss / nb_eval_steps
    results = {"loss": eval_loss}

    # Intent result
    if trainer.args.multilabel:
        intent_preds = (intent_preds >= 0.5).astype(float)
    else:
        intent_preds = np.argmax(intent_preds, axis=1)

    # Slot result
    if not trainer.args.use_crf:
        slot_preds = np.argmax(slot_preds, axis=2)
    slot_label_map = {i: label for i, label in enumerate(trainer.slot_label_lst)}
    out_slot_label_list = [[] for _ in range(out_slot_labels_ids.shape[0])]
    slot_preds_list = [[] for _ in range(out_slot_labels_ids.shape[0])]

    for i in range(out_slot_labels_ids.shape[0]):
        for j in range(out_slot_labels_ids.shape[1]):
            if out_slot_labels_ids[i, j] != trainer.pad_token_label_id:
                out_slot_label_list[i].append(slot_label_map[out_slot_labels_ids[i][j]])
                slot_preds_list[i].append(slot_label_map[slot_preds[i][j]])

    total_result = compute_metrics(
        intent_preds, out_intent_label_ids, slot_preds_list, out_slot_label_list, multilabel=multilabel
    )
    results.update(total_result)

    logger.info("***** Eval results *****")
    for key in sorted(results.keys()):
        logger.info("  %s = %s", key, str(results[key]))
    
    return results


class Trainer(object):
    def __init__(self, args, train_dataset=None, dev_dataset=None, test_dataset=None):
        self.args = args
        self.train_dataset = train_dataset
        self.dev_dataset = dev_dataset
        self.test_dataset = test_dataset

        self.intent_label_lst = get_intent_labels(args)
        self.slot_label_lst = get_slot_labels(args)

        # Use cross entropy ignore index as padding label id so that only real label ids contribute to the loss later
        self.pad_token_label_id = args.ignore_index

        self.config_class, self.model_class, _ = MODEL_CLASSES[args.model_type]
        if args.prev_model_dir == None:
            logger.info("***** Model Loaded From HuggingFace *****")
            self.config = self.config_class.from_pretrained(args.model_name_or_path, finetuning_task=args.task)
            self.model = self.model_class.from_pretrained(args.model_name_or_path,
                                                        config=self.config,
                                                        args=args,
                                                        intent_label_lst=self.intent_label_lst,
                                                        slot_label_lst=self.slot_label_lst)
        else:
            logger.info("***** Model Loaded From Files *****")
            self.config = self.config_class.from_pretrained(os.path.join(self.args.abs_path, self.args.prev_model_dir),
            finetuning_task=args.task)
            self.model = self.model_class.from_pretrained(os.path.join(self.args.abs_path, self.args.prev_model_dir),
                args=self.args,
                intent_label_lst=self.intent_label_lst,
                slot_label_lst=self.slot_label_lst,
                ignore_mismatched_sizes=True)

        # GPU or CPU
        self.device = "cuda" if torch.cuda.is_available() and not args.no_cuda else "cpu"
        self.model.to(self.device)

    def save_model(self):
        # Save model checkpoint (Overwrite)
        if not os.path.exists(os.path.join(self.args.abs_path, self.args.model_dir)):
            os.makedirs(os.path.join(self.args.abs_path, self.args.model_dir))
        model_to_save = (self.model.module if hasattr(self.model, "module") else self.model)
        model_to_save.save_pretrained(os.path.join(self.args.abs_path, self.args.model_dir))

        # Save training arguments together with the trained model
        torch.save(self.args, os.path.join(self.args.abs_path, self.args.model_dir, "training_args.bin"))
        logger.info("Saving model checkpoint to %s", self.args.model_dir)

    def load_model(self):
        # Check whether model exists
        if not os.path.exists(os.path.join(self.args.abs_path, self.args.model_dir)):
            raise Exception("Model doesn't exists! Train first!")

        try:
            self.model = self.model_class.from_pretrained(os.path.join(self.args.abs_path, self.args.model_dir), args=self.args,
                intent_label_lst=self.intent_label_lst, slot_label_lst=self.slot_label_lst)
            self.model.to(self.device)
            logger.info("***** Model Loaded *****")
        except:
            raise Exception("Some model files might be missing...")
